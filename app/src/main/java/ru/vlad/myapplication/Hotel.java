package ru.vlad.myapplication;

import androidx.annotation.Nullable;

import java.util.List;

public class Hotel {
    private int id;
    private String name;
    private String city;
    private List<Room> roomList;
    private byte[] imageSrc;

    public Hotel(int id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public List<Room> getRoomList() {
        return roomList;
    }

    public byte[] getImageSrc() {
        return imageSrc;
    }

    public void setRoomList(List<Room> roomList) {
        this.roomList = roomList;
        for (Room aRoom :
                roomList) {
            aRoom.setHotel(this);
        }
    }

    public void setImageSrc(byte[] imageSrc) {
        this.imageSrc = imageSrc;
    }

    @Nullable
    public Room getRoom(int number) {
        for (Room aRoom :
                roomList) {
            return aRoom;
        }
        return null;
    }
}

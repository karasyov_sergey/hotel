package ru.vlad.myapplication;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.HotelView> {

    private List<Hotel> hotelList;
    private Context context;

    public HotelAdapter(Context context) {
        hotelList = SQLVeteranDB.getDatabase().getHotelList();
        this.context = context;
    }

    @NonNull
    @Override
    public HotelView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HotelView(LayoutInflater.from(context).inflate(R.layout.hotel_element, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull HotelView holder, int position) {
        Hotel hotel = hotelList.get(position);
        holder.id = hotel.getId();
        holder.hotelName.setText(hotel.getName());
        holder.hotelCity.setText(hotel.getCity());

        Glide.with(context).asBitmap().load(hotel.getImageSrc()).into(holder.hotelImg);

    }

    @Override
    public int getItemCount() {
        return hotelList.size();
    }

    protected class HotelView extends RecyclerView.ViewHolder implements View.OnClickListener {
        private int id;
        private ImageView hotelImg;
        private TextView hotelName;
        private TextView hotelCity;

        public HotelView(@NonNull View itemView) {
            super(itemView);
            hotelImg = itemView.findViewById(R.id.hotel_img);
            hotelName = itemView.findViewById(R.id.hotel_name);
            hotelCity = itemView.findViewById(R.id.hotel_city);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, RoomActivity.class);
            intent.putExtra(IntentKeys.ROOM_ID_KEY, id);
            context.startActivity(intent);
        }
    }
}

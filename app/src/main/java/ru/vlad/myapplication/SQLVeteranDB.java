package ru.vlad.myapplication;

import android.content.Context;

import androidx.annotation.Nullable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SQLVeteranDB {
    private static final String IP = "192.168.0.104";
    private static final String PORT = "49172";
    private static final String DRIVER = "net.sourceforge.jtds.jdbc.Driver";
    private static final String DB = "VeteranDB";
    private static final String USER = "test";
    private static final String PASSWORD = "test";
    private static final String URL = "jdbc:jtds:sqlserver://" + IP + ":" + PORT + "/" + DB;
    public static final String GET_VETERAN_DATA = "EXEC get_veteran_data '%s';";
    public static final String SELECT_ROOM_LIST = "EXEC room_select;";
    public static final String SELECT_HOTEL_LIST = "EXEC hotel_select";
    public static final String CONTAINS_VETERAN = "EXEC contains_veteran '%s';";
    public static final String TAKE_ROOM = "EXEC take_room_space '%s', %d;";
    public static final String CANCEL_TAKE_ROOM = "EXEC cancel_book '%s', %d;";
    private static SQLVeteranDB database;

    private List<Hotel> hotelList;
    private List<Room> roomList;
    private Account account;

    private SQLVeteranDB() {
        hotelList = new ArrayList<>();
        roomList = new ArrayList<>();
    }

    public static SQLVeteranDB getDatabase() {
        return database;
    }

    public static void createDataBase(Context context) {
        if (database != null) {
            return;
        }
        database = new SQLVeteranDB();
        try (Connection connection = getConnection()) {
            initRoomList(connection);
            initHotelList(connection);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            ToastMaker.showToast(context, R.string.connection_error);
        }
    }

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }

    private static void initHotelList(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet set = statement.executeQuery(SELECT_HOTEL_LIST);
        while (set.next()) {
            Hotel hotel = new Hotel(set.getInt("id"), set.getString("name"), set.getString("city"));
            hotel.setImageSrc(set.getBytes("img"));
            hotel.setRoomList(getHotelRooms(hotel.getId()));
            database.getHotelList().add(hotel);
        }
    }

    private static List<Room> getHotelRooms(int id) {
        List<Room> list = new ArrayList<>();
        for (Room aRoom :
                database.getRoomList()) {
            if (aRoom.getHotelId() == id) {
                list.add(aRoom);
            }
        }
        return list;
    }

    private static void initRoomList(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet set = statement.executeQuery(SELECT_ROOM_LIST);
        while (set.next()) {
            Room room = new Room(set.getInt("id"), set.getInt("number"), set.getInt("general_space"), set.getInt("free_space"));
            room.setHotelId(set.getInt("hotel_id"));
            database.getRoomList().add(room);
        }
    }

    public List<Hotel> getHotelList() {
        return hotelList;
    }

    public List<Room> getRoomList() {
        return roomList;
    }

    @Nullable
    public Hotel getHotelById(int id) {
        for (Hotel aHotel :
                hotelList) {
            if (aHotel.getId() == id) {
                return aHotel;
            }
        }
        return null;
    }

    public Account getAccount() {
        return account;
    }

    public void initAccount(Context context, String id) {
        try (Connection connection = getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(String.format(Locale.ENGLISH, GET_VETERAN_DATA, id));
            if (set.next()) {
                account = new Account(id, set.getString("name"), set.getString("surname"), set.getString("middlename"));
                account.setAgeGroup(AgeGroup.getAgeGroup(set.getInt("age_group")));
                account.setCity(set.getString("city"));
                account.setSport(set.getString("sport"));
                account.setHotelName(set.getString("hotel_name"));
                Integer number = set.getInt("room_number");
                account.setRoomNumber(number);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            ToastMaker.showToast(context, R.string.connection_error);
        }
    }

    @Nullable
    public Room getRoom(String hotelName, int number) {
        Room room = null;
        for (Hotel aHotel :
                hotelList) {
            if (aHotel.getName().equals(hotelName)) {
                room = aHotel.getRoom(number);
            }
        }
        return room;
    }
}

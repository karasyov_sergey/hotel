package ru.vlad.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Locale;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.RoomView> {
    private List<Room> roomList;
    private Context context;
    private SharedPreferences pref;

    public RoomAdapter(Context context, List<Room> roomList) {
        this.roomList = roomList;
        this.context = context;
        pref = context.getSharedPreferences(PreferencesKeys.PREF_NAME, Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public RoomView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RoomView(LayoutInflater.from(context).inflate(R.layout.room_element, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RoomView holder, int position) {
        holder.room = roomList.get(position);
        holder.setDataView();
    }

    @Override
    public int getItemCount() {
        return roomList.size();
    }

    protected class RoomView extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Room room;
        private TextView number;
        private TextView freeSpace;
        private TextView generalSpace;

        public RoomView(@NonNull View itemView) {
            super(itemView);
            number = itemView.findViewById(R.id.room_number);
            generalSpace = itemView.findViewById(R.id.general_space);
            freeSpace = itemView.findViewById(R.id.free_space);
            Button toBookBtn = itemView.findViewById(R.id.to_book_btn);
            toBookBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Account account = SQLVeteranDB.getDatabase().getAccount();
            int freeSpc = room.getFreeSpace();
            if (freeSpc == 0) {
                ToastMaker.showToast(context, R.string.free_out);
                return;
            }
            try (Connection connection = SQLVeteranDB.getConnection()) {
                Statement statement = connection.createStatement();
                String veteranID = pref.getString(PreferencesKeys.VETERAN_ID_KEY, "");
                int result = statement.executeUpdate(String.format(Locale.ENGLISH, SQLVeteranDB.TAKE_ROOM,
                        veteranID, room.getId()));
                if (result == 1 && account.getHotelName() == null) {
                    room.setFreeSpace(--freeSpc);
                    notifyDataSetChanged();
                    account.setRoomNumber(room.getNumber());
                    account.setHotelName(room.getHotel().getName());
                    ToastMaker.showToast(context, R.string.book_has_been_successfully);
                    return;
                }
                ToastMaker.showToast(context, R.string.you_have_room);
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
                ToastMaker.showToast(context, R.string.connection_error);
            }
        }

        public void setDataView() {
            if (room == null) {
                return;
            }
            number.setText(String.format(Locale.ENGLISH, context.getString(R.string.number), room.getNumber()));
            freeSpace.setText(String.format(Locale.ENGLISH, context.getString(R.string.free_space), room.getFreeSpace()));
            generalSpace.setText(String.format(Locale.ENGLISH, context.getString(R.string.general_space), room.getGeneralSpace()));
        }
    }
}

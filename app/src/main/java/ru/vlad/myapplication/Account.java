package ru.vlad.myapplication;

public class Account {
    private String id;
    private String name;
    private String surname;
    private String middleName;
    private AgeGroup ageGroup;
    private String city;
    private String sport;
    private String hotelName;
    private Integer roomNumber;

    public Account(String id, String name, String surname, String middleName) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public String getCity() {
        return city;
    }

    public String getSport() {
        return sport;
    }

    public String getHotelName() {
        return hotelName;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setAgeGroup(AgeGroup ageGroup) {
        this.ageGroup = ageGroup;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getId() {
        return id;
    }
}

package ru.vlad.myapplication;

import androidx.annotation.NonNull;

public enum AgeGroup {
    YOUNG("Молодой", 18, 44),
    MIDDLE("Средний", 45, 59),
    AGED("Пожилой", 60, 74),
    OLD("Старый", 75, 89),
    ANCIENT("Долгожитель", 90),
    UNKNOWN("Неизвестный");

    private static AgeGroup[] ageGroups = {
            YOUNG,
            MIDDLE,
            AGED,
            OLD,
            ANCIENT
    };
    private String name;
    private int min;
    private int max;

    AgeGroup(String name, int min, int max) {
        this.name = name;
        this.min = min;
        this.max = max;
    }

    AgeGroup(String name, int from) {
        this.name = name;
        this.min = from;
        this.max = Integer.MAX_VALUE;
    }

    AgeGroup(String name) {
        this.name = name;
        this.min = 0;
        this.max = 0;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public static AgeGroup getAgeGroup(int age) {
        for (AgeGroup aGroup :
                ageGroups) {
            if (aGroup.inRanged(age)) {
                return aGroup;
            }
        }
        return UNKNOWN;
    }

    private boolean inRanged(int age) {
        return age >= min && age <= max;
    }
}

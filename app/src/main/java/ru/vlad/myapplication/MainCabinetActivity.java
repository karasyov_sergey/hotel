package ru.vlad.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

public class MainCabinetActivity extends AppCompatActivity implements View.OnClickListener {

    private Account account;
    private SQLVeteranDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_cabinet);
        db = SQLVeteranDB.getDatabase();
        account = db.getAccount();
        TextView fullName = findViewById(R.id.full_name);
        TextView ageGroup = findViewById(R.id.age_group);
        TextView city = findViewById(R.id.city);
        TextView sport = findViewById(R.id.sport);
        TextView hotelName = findViewById(R.id.hotel_name);
        TextView roomNumber = findViewById(R.id.room_number);

        fullName.setText(String.format(Locale.ENGLISH, getString(R.string.full_name),
                account.getSurname(), account.getName(), account.getMiddleName()));

        String ageGroupData = account.getAgeGroup().getName();
        ageGroup.setText(String.format(Locale.ENGLISH, getString(R.string.age_group), ageGroupData));

        String cityData = !account.getCity().equals("city") ? account.getCity() : getString(R.string.not_exist);
        city.setText(String.format(Locale.ENGLISH, getString(R.string.city), cityData));

        String sportData = !account.getSport().equals("sport") ? account.getSport() : getString(R.string.not_exist);
        sport.setText(String.format(Locale.ENGLISH, getString(R.string.sport), sportData));

        String hotelNameData = account.getHotelName() != null ? account.getHotelName() : getString(R.string.not_exist);
        hotelName.setText(String.format(Locale.ENGLISH, getString(R.string.hotel_name), hotelNameData));

        String roomNumberData = account.getHotelName() != null ? account.getRoomNumber().toString() : getString(R.string.not_exist);
        roomNumber.setText(String.format(Locale.ENGLISH, getString(R.string.room_number), roomNumberData));

        Button cancelBtn = findViewById(R.id.to_cancel_book_btn);
        cancelBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (account.getHotelName() == null) {
            ToastMaker.showToast(this, R.string.you_have_not_room);
            return;
        }
        Room room = db.getRoom(account.getHotelName(), account.getRoomNumber());
        if (room == null) {
            ToastMaker.showToast(this, R.string.you_have_not_room);
            return;
        }
        try (Connection connection = SQLVeteranDB.getConnection()) {
            Statement statement = connection.createStatement();
            Log.d("acc", "hello");
            statement.executeUpdate(String.format(Locale.ENGLISH, SQLVeteranDB.CANCEL_TAKE_ROOM, account.getId(), account.getRoomNumber()));
            ToastMaker.showToast(this, R.string.you_have_been_cancel_book);
            room.setFreeSpace(room.getFreeSpace() + 1);
            account.setHotelName(null);
            account.setRoomNumber(null);
            startActivity(new Intent(this, HotelActivity.class));
            finish();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            ToastMaker.showToast(this, R.string.connection_error);
        }
    }
}

package ru.vlad.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RoomActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        Intent intent = getIntent();
        int hotelId = intent.getIntExtra(IntentKeys.ROOM_ID_KEY, -1);
        Hotel hotel = SQLVeteranDB.getDatabase().getHotelById(hotelId);
        if (hotel != null) {
            RecyclerView recyclerView = findViewById(R.id.room_list);
            RoomAdapter adapter = new RoomAdapter(this, hotel.getRoomList());
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        MenuItem item = menu.findItem(R.id.main_cabinet);
        item.setOnMenuItemClickListener(this);
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        startActivity(new Intent(this, MainCabinetActivity.class));
        return true;
    }
}

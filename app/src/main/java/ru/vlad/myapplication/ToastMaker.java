package ru.vlad.myapplication;

import android.content.Context;
import android.widget.Toast;

public class ToastMaker {

    public static void showToast(Context context, int stringId) {
        Toast.makeText(context, context.getString(stringId), Toast.LENGTH_LONG).show();
    }
}

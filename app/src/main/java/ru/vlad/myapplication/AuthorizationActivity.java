package ru.vlad.myapplication;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

public class AuthorizationActivity extends AppCompatActivity implements TextView.OnEditorActionListener {
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.INTERNET},
                PackageManager.PERMISSION_GRANTED);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        EditText guidInput = findViewById(R.id.guid_input);
        guidInput.setOnEditorActionListener(this);
        SQLVeteranDB.createDataBase(this);

        pref = getSharedPreferences(PreferencesKeys.PREF_NAME, MODE_PRIVATE);
        if (pref.contains(PreferencesKeys.VETERAN_ID_KEY)) {
            String id = pref.getString(PreferencesKeys.VETERAN_ID_KEY, null);
            SQLVeteranDB.getDatabase().initAccount(this, id);
            toHotelActivity();
        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            String id = textView.getText().toString();
            if (!isContained(id)) {
                return false;
            }
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(PreferencesKeys.VETERAN_ID_KEY, id);
            editor.apply();
            SQLVeteranDB.getDatabase().initAccount(this, id);
            toHotelActivity();

        }
        return false;
    }

    private void toHotelActivity() {
        startActivity(new Intent(this, HotelActivity.class));
        finish();
    }

    private boolean isContained(CharSequence id) {
        boolean contains = false;
        try (Connection connection = SQLVeteranDB.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet set = statement.executeQuery(String.format(Locale.ENGLISH, SQLVeteranDB.CONTAINS_VETERAN, id));
            contains = set.next();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            ToastMaker.showToast(this, R.string.connection_error);
        }
        return contains;
    }
}
package ru.vlad.myapplication;

public class Room {
    private Hotel hotel;
    private int id;
    private int number;
    private int generalSpace;
    private int freeSpace;
    private int hotelId;

    public Room(int id, int number, int generalSpace, int freeSpace) {
        this.id = id;
        this.number = number;
        this.generalSpace = generalSpace;
        this.freeSpace = freeSpace;
    }

    public int getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public int getGeneralSpace() {
        return generalSpace;
    }

    public int getFreeSpace() {
        return freeSpace;
    }

    public int getHotelId() {
        return hotelId;
    }

    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    public void setFreeSpace(int freeSpace) {
        this.freeSpace = freeSpace;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}

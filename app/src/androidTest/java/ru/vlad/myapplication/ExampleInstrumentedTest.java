package ru.vlad.myapplication;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Before
    public void init() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        SQLVeteranDB.createDataBase(appContext);
    }

    @Test
    public void doDBNullTest() {
        assertNotNull(SQLVeteranDB.getDatabase());
    }

    @Test
    public void doGHotelNullTest() {
        assertNotNull(SQLVeteranDB.getDatabase().getHotelList());
    }

    @Test
    public void doRoomNullTest() {
        assertNotNull(SQLVeteranDB.getDatabase().getRoomList());
    }

    @Test
    public void doHotelImgNullTest() {
        List<Hotel> hotelList = SQLVeteranDB.getDatabase().getHotelList();
        for (Hotel aHotel :
                hotelList) {
            if (aHotel.getImageSrc() == null) {
                fail();
                return;
            }
        }
    }

    @Test
    public void doRoomListNullTest() {
        List<Hotel> hotelList = SQLVeteranDB.getDatabase().getHotelList();
        for (Hotel aHotel :
                hotelList) {
            if (aHotel.getRoomList() == null) {
                fail();
                return;
            }
        }
    }
}